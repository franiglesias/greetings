FROM openjdk:17-alpine3.14

RUN apk update && apk upgrade &&\
    mkdir /app &&\
    adduser -D javauser -u 1001 &&\
    chown -R javauser /app

COPY --chown=javauser:javauser target/*.jar /app/app.jar

WORKDIR /app

USER javauser

EXPOSE 8080

ENTRYPOINT ["java","-jar","app.jar"]